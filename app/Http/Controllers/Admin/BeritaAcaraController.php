<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CerapanSpbu;
use App\Models\BeritaAcara;
use App\Models\SuratTugasDetails;
use Illuminate\Http\Request;
use App\Models\SuratTugas;
use App\Models\MSpbu;
use DataTables;
use Carbon\Carbon;
use PDF;

class BeritaAcaraController extends Controller
{
    public function index(Request $request) {
        $data = BeritaAcara::latest()->get();
        if ($request->ajax()) {
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                if (auth()->user()->role->nama == 'kepala-bidang') {
                    if ($row->status == 'klarifikasi' || $row->status == 'segel' || $row->status == 'perpanjang') {
                        $btn = '<a href="'.route('admin.berita-acara.details', $row->id).'" class="btn btn-sm btn-icon btn-info rounded-pill btn-icon me-2 detailsBerita" data-id="'.$row->id.'"><i class="mdi mdi-information-variant mdi-10px"></i></a>';
                    }else{
                        $btn = '<a href="'.route('admin.berita-acara.approve', $row->id).'" class="btn btn-sm btn-icon btn-success rounded-pill btn-icon me-2 approveId" data-id="'.$row->id.'"><i class="mdi mdi-plus mdi-10px"></i></a>';
                    }
                } else {
                    $btn = '<a href="'.route('admin.berita-acara.details', $row->id).'" class="btn btn-sm btn-icon btn-info rounded-pill btn-icon me-2 detailsBerita" data-id="'.$row->id.'"><i class="mdi mdi-information-variant mdi-10px"></i></a>';
                }
                $btn = $btn .'<a href="'.route('admin.berita-acara.pdf', $row->id).'" class="btn btn-sm btn-icon btn-secondary rounded-pill btn-icon me-2 donwnloadPdf" data-id="'.$row->id.'" target="_blank"><i class="mdi mdi-file-document mdi-10px"></i></a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('admin.berita-acara.index');
    }

    public function pdf($id){
        $beritaAcara = BeritaAcara::with('surat_tugas', 'cerapan_spbu')->where('id', $id)->first();
        $perusahaan = MSpbu::where('kode_spbu', $beritaAcara->kode_perusahaan)->first();
        $staff = SuratTugasDetails::where('t_surat_tugas_spbu_id', $beritaAcara->surat_tugas->id)->get();
        $pdf = PDF::loadview('admin.berita-acara.pdf', [
            'berita_acara' => $beritaAcara,
            'perusahaan' => $perusahaan,
            'staff' => $staff
        ]);

        return $pdf->stream();
    }

    public function approve(Request $request, $id){
        $berita = BeritaAcara::where('id', $id)->first();
        $perusahaan = MSpbu::where('kode_spbu', $berita->kode_perusahaan)->first();
        $tglSertifikat = Carbon::parse($perusahaan->tanggal_sertifikat_uttp."00:00:00");
        $tglExpired = Carbon::parse($perusahaan->expired_date."00:00:00");
        $dateNow = Carbon::now()->format('Y-m-d');

        $selisih = $tglExpired->diffInDays($tglSertifikat);

        if ($selisih <= 100) {
            $status = 'Limit '. $selisih. ' Hari';
        } else {
            if ($dateNow > $tglExpired) {
                $status = 'expired';
                $afterDate = $tglExpired->diffInDays($dateNow);
            }else{
                $status = 'perpanjang';
                $afterDate = $tglExpired->diffInDays($dateNow);
            }
        }

        $suratTugas = SuratTugas::with('TSuratTugasDetails')->where('id', $berita->surat_tugas_id)->first();
        $cerapan = CerapanSpbu::where('berita_acara_spbu_id', $berita->id)->get();
        return view('admin.berita-acara.approve', [
            'berita' => $berita,
            'perusahaan' => $perusahaan,
            'status' => $status,
            'after_date' => $afterDate,
            'surat' => $suratTugas,
            'cerapan' => $cerapan,
        ]);
    }


    public function details(Request $request, $id){
        $berita = BeritaAcara::where('id', $id)->first();
        $perusahaan = MSpbu::where('kode_spbu', $berita->kode_perusahaan)->first();
        $tglSertifikat = Carbon::parse($perusahaan->tanggal_sertifikat_uttp."00:00:00");
        $tglExpired = Carbon::parse($perusahaan->expired_date."00:00:00");
        $dateNow = Carbon::now();

        $selisih = $dateNow->diffInDays($tglExpired);

        if ($selisih <= 100) {
            $status = 'Limit '. $selisih. ' Hari';
        } else {
            if ($dateNow > $tglExpired) {
                $status = 'expired';
                $afterDate = $tglExpired->diffInDays($dateNow);
            }else{
                $status = 'perpanjang';
                $afterDate = $tglExpired->diffInDays($dateNow);
            }
        }

        $suratTugas = SuratTugas::with('TSuratTugasDetails')->where('id', $berita->surat_tugas_id)->first();
        $cerapan = CerapanSpbu::where('berita_acara_spbu_id', $berita->id)->get();
        return view('admin.berita-acara.details', [
            'berita' => $berita,
            'perusahaan' => $perusahaan,
            'status' => $status,
            'after_date' => $afterDate,
            'surat' => $suratTugas,
            'cerapan' => $cerapan,
        ]);
    }

    public function approveStore(Request $request){

        $expired = Carbon::now()->addYears(2);

        $berita = BeritaAcara::where('id', $request->berita_acara_id)->first();
        $berita->status = $request->status;
        $berita->keterangan = $request->keterangan;
        $berita->save();

        if (!empty($request->status)) {
            $spbu = MSpbu::where('kode_spbu', $berita->kode_perusahaan)->first();
            $spbu->status = $request->status;
            if ($request->status == 'perpanjang') {
                $spbu->tanggal_sertifikat_uttp = Carbon::now()->format('Y-m-d');
                $spbu->expired_date = Carbon::parse($expired)->format('Y-m-d');
            }
            $spbu->save();
        }

        // update status surat tugas
        $surat = SuratTugas::where('id', $berita->surat_tugas_id)->first();
        $surat->status = $request->status;
        $surat->save();

        return redirect()->route('admin.berita-acara.index');
    }
}
