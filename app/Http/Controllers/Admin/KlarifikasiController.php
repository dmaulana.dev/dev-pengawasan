<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TBeritaAcara;
use DataTables;

class KlarifikasiController extends Controller
{
    public function index(Request $request) {
        $data = TBeritaAcara::where('status', 'klarifikasi')->latest()->get();
        if ($request->ajax()) {
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                if (auth()->user()->role->nama == 'kepala-bidang') {
                    if ($row->status == 'klarifikasi') {
                        $btn = '<a href="'.route('admin.berita-acara.details', $row->id).'" class="btn btn-sm btn-icon btn-info rounded-pill btn-icon me-2 detailsBerita" data-id="'.$row->id.'"><i class="mdi mdi-information-variant mdi-10px"></i></a>';
                        $btn = '<a href="'.route('admin.berita-acara.approve', $row->id).'" class="btn btn-sm btn-icon btn-success rounded-pill btn-icon me-2 approveId" data-id="'.$row->id.'"><i class="mdi mdi-plus mdi-10px"></i></a>';
                    }else{
                        $btn = '<a href="'.route('admin.berita-acara.details', $row->id).'" class="btn btn-sm btn-icon btn-info rounded-pill btn-icon me-2 detailsBerita" data-id="'.$row->id.'"><i class="mdi mdi-information-variant mdi-10px"></i></a>';
                    }
                } else {
                    $btn = '<a href="'.route('admin.berita-acara.details', $row->id).'" class="btn btn-sm btn-icon btn-info rounded-pill btn-icon me-2 detailsBerita" data-id="'.$row->id.'"><i class="mdi mdi-information-variant mdi-10px"></i></a>';
                }
                $btn = $btn .'<a href="'.route('admin.berita-acara.pdf', $row->id).'" class="btn btn-sm btn-icon btn-secondary rounded-pill btn-icon me-2 donwnloadPdf" data-id="'.$row->id.'" target="_blank"><i class="mdi mdi-file-document mdi-10px"></i></a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }

        return view('admin.klarifikasi.index');
    }
}
