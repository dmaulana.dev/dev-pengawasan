<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MMinumanAlkohol;
use DataTables;
use Carbon\Carbon;

class MAlkoholController extends Controller
{
    public function index(Request $request) {
        $data = MMinumanAlkohol::latest()->get();

        $data = $data->map(function ($dt) {
            $tglSertifikat = Carbon::parse($dt->tanggal_sertifikat_uttp."00:00:00");
            $tglExpired = Carbon::parse($dt->expired_date."00:00:00");
            $dateNow = Carbon::now();

            $selisih = $dateNow->diffInDays($tglExpired);

            if ($selisih <= 100) {
                $status = 'Limit '. $selisih. ' Hari';
            } else {
                if ($dateNow > $tglExpired) {
                    $status = 'expired';
                }else{
                    $status = 'perpanjang';
                }
            }

            return [
                'id' => $dt->id,
                'kode' => $dt->kode,
                'nama' => $dt->nama,
                'alamat' => $dt->alamat,
                'nama_pengelola' => $dt->nama_pengelola,
                'nomor_hp' => $dt->nomor_hp,
                'nomor_nib' => $dt->nomor_nib,
                'tanggal_nib' => $dt->tanggal_nib,
                'nomor_sertifikat_uttp' => $dt->nomor_sertifikat_uttp,
                'tanggal_sertifikat_uttp' => $dt->tanggal_sertifikat_uttp,
                'email' => $dt->email,
                'status' => $status,
            ];
        });

        if ($request->ajax()) {
            return Datatables::of(json_decode($data))
            ->addIndexColumn()
            ->addColumn('action', function($row){
                if (auth()->user()->role_nama == 'admin') {
                    $btn = '<button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2 editSpbu" data-id="'.$row->id.'"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>';
                    $btn = $btn.'<a href="'.route('admin.spbu.destroy', $row->id).'" class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon delete-record"><i class="mdi mdi-delete-outline mdi-20px"></i></a>';
                }else{
                    $btn = '<button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2 editSpbu" data-id="'.$row->id.'"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>';
                }
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('admin.master.m_alkohol');
    }
}
