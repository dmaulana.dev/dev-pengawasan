<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MSpbu;
use App\Models\MSarana;

use DataTables;
use Carbon\Carbon;

class MSaranaController extends Controller
{
    public function index(Request $request){
        $data = MSarana::latest()->get();

        if ($request->ajax()) {
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2 editSarana" data-id="'.$row->id.'"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>';
                $btn = $btn.'<a href="'.route('sarana.destroy', $row->id).'" class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon delete-record"><i class="mdi mdi-delete-outline mdi-20px"></i></a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('admin.sarana.index');

    }


    public function store(Request $request){
        $data = new MSarana;
        $data->kode = $request->kode;
        $data->nama = $request->nama;
        $data->jenis = $request->jenis;
        $data->qty = $request->qty;
        $data->satuan = $request->satuan;
        $data->save();

        return redirect()->route('sarana.index');
    }

    public function show(Request $request, $id){
        $data = MSarana::findOrFail($id);

        return response()->json([
            'type' => 'success',
            'data' => $data,
        ], 200);
    }

    public function update(Request $request){
        $data = MSarana::where('id', $request->sarana_id)->first();
        $data->kode = $request->kode;
        $data->nama = $request->nama;
        $data->jenis = $request->jenis;
        $data->qty = $request->qty;
        $data->satuan = $request->satuan;
        $data->save();

        return redirect()->route('sarana.index');
    }

    public function destroy($id){
        $data = MSarana::where('id', $id)->first();
        $data->delete();

        return redirect()->route('sarana.index');
    }


}
