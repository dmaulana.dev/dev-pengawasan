<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\ProfileStaff;
use App\Models\Role;
use App\Models\User;
use DataTables;

class MStaffController extends Controller
{
    public function index(Request $request){
        $data = ProfileStaff::latest()->get();
        if ($request->ajax()) {
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<button class="btn btn-icon btn-text-info rounded-pill btn-icon me-2 editStaff" data-id="'.$row->id.'"><i class="fa fa-pencil"></i></button>';
                $btn = $btn.'<button class="btn btn-icon btn-text-secondary rounded-pill btn-icon me-2 hapusStaff" data-id="'.$row->id.'"><i class="fa fa-trash"></i></button>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        $listRole = Role::get();
        return view('admin.master.m_staff', ['list_role' => $listRole]);
    }

    public function list(Request $request)
    {
        $mstaff = ProfileStaff::when($request->keyword, function($query) use ($request) {
            if (!empty($request->keyword)) {
                $query->where('nama_lengkap', 'like', '%'.$request->keyword.'%')
                ->orWhere('nomor_nip', 'like', '%'.$request->keyword.'%');
            }
        })
        ->take(10)
        ->get();

        return response()->json([
            'type' => 'success',
            'data' => $mstaff
        ], 200);
    }

    public function store(Request $request){
        $role = Role::where('id', $request->role)->first();
        $admin = new User;
        $admin->full_name = $request->nama_lengkap;
        $admin->username = $request->nomor_nip;
        $admin->password = Hash::make('09910991');
        $admin->role_id = $role->id;
        $admin->role_name = $role->name;
        $admin->active = 1;
        $admin->save();

        $staff = new ProfileStaff;
        $staff->user_id = $admin->id;
        $staff->profile_type = 'staff';
        $staff->nama_lengkap = $request->nama_lengkap;
        $staff->nomor_nip = $request->nomor_nip;
        $staff->email = $request->email;
        $staff->nomor_hp = $request->nomor_hp;
        $staff->jabatan = $request->jabatan;
        $staff->unit_kerja = $request->unit_kerja;
        $staff->active = 1;
        $staff->save();

        return redirect()->route('master-staff.index');
    }

    public function show(Request $request, $id){
        $data = ProfileStaff::where('id', $id)->first();
        return response()->json([
            'data' => $data,
            'type' => 'success'
        ], 200);
    }
}
