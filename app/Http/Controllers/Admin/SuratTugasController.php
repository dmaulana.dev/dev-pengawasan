<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MMinumanAlkohol;
use App\Models\ProfileAdmin;
use Illuminate\Http\Request;
use App\Models\MSpbu;
use App\Models\ProfileStaff;
use App\Models\SuratTugas;
use App\Models\SuratTugasDetails;
use App\Models\MSarana;
use App\Models\Role;
use App\Models\SuratTugasTemp;
use DataTables;
use Carbon\Carbon;

class SuratTugasController extends Controller
{
    public function index(Request $request) {
        $userId = Auth()->user()->id;
        $profilTipe = ProfileAdmin::where('user_id', $userId)->first();

        if ($profilTipe->profile_tipe == 'industri') {
            $tipe = 'spbu';
        } elseif ($profilTipe->profile_tipe == 'perdagangan') {
            $tipe = 'minol';
        }else{
            $datas = SuratTugas::with('SuratTugasDetails')->get();
        }

        $datas = SuratTugas::where('tipe_surat', $tipe)->with('SuratTugasDetails')->get();

        $data = $datas->map(function ($dt){
            return [
                'id' => $dt->id,
                'tanggal_surat' => $dt->tanggal_surat,
                'tanggal_pelaksanaan' => $dt->tanggal_pelaksanaan,
                'perusahaan_kode' => $dt->perusahaan_kode,
                'perusahaan_nama' => $dt->perusahaan_nama,
                'status' => $dt->status,
            ];
        });

        if ($request->ajax()) {
            return Datatables::of(json_decode($data))
            ->addIndexColumn()
            ->addColumn('action', function($row){
                // $btn = '<button class="btn btn-icon btn-text-secondary rounded-pill btn-icon me-2 addSuratTugas" data-id="'.$row->id.'"><i class="fa fa-file"></i></button>';
                $btn = '<a href="'.route('surat-tugas.details', $row->id).'" class="btn btn-sm btn-info rounded-pill btn-icon me-2"><i class="mdi mdi-information-variant mdi-10px"></i></a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }

        return view('admin.surat-tugas.index');
    }

    public function show(Request $request, $id){
        $data = SuratTugas::with('spbu')->where('id', $id)->first();
        return response()->json([
            'data' => $data,
            'type' => 'success'
        ], 200);
    }


    public function details(Request $request, $id_surat){
        $data = SuratTugas::with('SuratTugasDetails')->where('id', $id_surat)->first();
        $perusahaan  = MSpbu::where('id', $data->perusahaan_id)->first();
        return view('admin.surat-tugas.details', [
            'data' => $data,
            'perusahaan' => $perusahaan
        ]);
    }

    public function getSpbu(Request $request){
        $spbu = MSpbu::latest()->get();

        $data = $spbu->map(function ($dt) {
            $tglSertifikat = Carbon::parse($dt->tanggal_sertifikat_uttp."00:00:00");
            $tglExpired = Carbon::parse($dt->expired_date."00:00:00");
            $dateNow = Carbon::now();

            $selisih = $dateNow->diffInDays($tglExpired);

            if ($selisih <= 100) {
                $status = 'Limit '. $selisih. ' Hari';
            } else {
                if ($dateNow > $tglExpired) {
                    $status = 'expired';
                }else{
                    $status = 'perpanjang';
                }
            }

            return [
                'id' => $dt->id,
                'kode_spbu' => $dt->kode_spbu,
                'nama_spbu' => $dt->nama_spbu,
                'alamat' => $dt->alamat,
                'nama_pengelola' => $dt->nama_pengelola,
                'nomor_hp' => $dt->nomor_hp,
                'nomor_sertifikat_uttp' => $dt->nomor_sertifikat_uttp,
                'email' => $dt->kode_spbu,
                'status' => $status,
            ];
        });

        if ($request->ajax()) {
            return Datatables::of(json_decode($data))
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<button type="button" class="btn btn-sm btn-primary addStaff" data-id="'.$row->id.'"><i class="mdi mdi-plus-outline mdi-10px"></i> Pilih</button>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function create(Request $request){
        $userId = auth()->user()->id;
        $profile = ProfileAdmin::where('user_id', $userId)->first();

        $listSarana = MSarana::where('qty', '>', 0)->latest()->get();
        $staff = ProfileStaff::where('active', 1)
        ->where('profile_tipe', $profile->profile_tipe)
        ->latest()
        ->get();

        $temp = SuratTugasTemp::where('visible', 1)->get();
        $suratTugasTemp = $temp->groupBy('perusahaan_id')->map(function ($q) use ($profile) {
            if ($profile->profile_tipe == 'industri') {
                $perusahaan = MSpbu::where('id', $q->first()->perusahaan_id)->first();
            } else {
                $perusahaan = MMinumanAlkohol::where('id', $q->first()->perusahaan_id)->first();
            }

            return [
                'perusahaan_id' => $perusahaan->id,
                'perusahaan_kode' => !empty($perusahaan->kode_spbu) ?  $perusahaan->kode_spbu :  $perusahaan->kode,
                'perusahaan_nama' => !empty($perusahaan->nama_spbu) ?  $perusahaan->nama_spbu :  $perusahaan->nama,
                'pengelola' => $perusahaan->nama_pengelola,
                'nomor_hp' => $perusahaan->nomor_hp,
                'nomor_nib' => $perusahaan->nomor_nib,
                'jumlah_staff' => count($q),
            ];
        })
        ->values();

        return view('admin.surat-tugas.create', [
            'sarana' => $listSarana,
            'staff' => $staff,
            'temp' => $suratTugasTemp
        ]);
    }

    public function tempDetails(Request $request, $id){
        $spbu = SuratTugasTemp::where('perusahaan_id', $id)->where('visible', 1)->latest()->get();
        if ($request->ajax()) {
            return Datatables::of($spbu)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<a href="'.route('surat-tugas.temp-hapus', $row->id).'" class="btn btn-sm btn-danger"><i class="mdi mdi-delete-forever mdi-10px"></i></a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }


    public function tempHapus(Request $request, $id){
        $data = SuratTugasTemp::where('id', $id)->first();
        $data->delete();

        return redirect()->route('surat-tugas.create');
    }


    public function temp(Request $request){
        foreach ($request['nomor_nip'] as $key => $value) {
            $staff = ProfileStaff::where('id', $request['staff_id'])->first();
            $temp = new suratTugasTemp;
            $temp->perusahaan_id = $request['perusahaan_id'];
            $temp->staff_id = $staff->id;
            $temp->nomor_nip = $request['nomor_nip'][$key];
            $temp->nama_staff = $staff->nama_lengkap;
            $temp->nomor_hp = $staff->nomor_hp;
            $temp->golongan = $staff->golongan;
            $temp->jabatan = $request['jabatan'][$key];
            $temp->unit_kerja = $request['unit_kerja'][$key];
            $temp->created_by = auth()->user()->id;
            $temp->visible = 1;
            $temp->save();
        }

        return redirect()->back();
    }


    public function store(Request $request){
        $reqSarana = $request->sarana;
        foreach ($reqSarana as $key => $value) {
            $sarana = MSarana::where('kode', $value)->first();
            $sarana->qty = $sarana->qty - 1;
            $sarana->save();
        }
        $implode = MSarana::whereIn('kode', $reqSarana)->get();
        $listSarana  = $implode->pluck('kode')->implode(',');

        foreach ($request['perusahaan_id'] as $key => $perusahaanId) {
            $surat = new SuratTugas;
            $surat->perusahaan_id = $perusahaanId;
            $surat->perusahaan_kode = $request['perusahaan_kode'][$key];
            $surat->perusahaan_nama = $request['perusahaan_nama'][$key];
            $surat->tipe_surat = 'spbu';
            $surat->nomor_surat = $request['nomor_surat'];
            $surat->tanggal_surat = $request['tanggal_surat'];
            $surat->objek_pengawasan = $request['objek_pengawasan'];
            $surat->tanggal_pelaksanaan = $request['tanggal_pelaksanaan'];
            $surat->perihal = $request['perihal'];
            $surat->tentang = $request['tentang'];
            $surat->kode_sarana = $listSarana;
            $surat->status = 'penugasan';
            $surat->created_by = auth()->user()->id;
            $surat->save();

            $data = SuratTugasTemp::where('perusahaan_id', $request['perusahaan_id'][$key])->where('visible', 1)->get();
            foreach ($data as $value) {
                $spbu = MSpbu::where('id', $value->perusahaan_id)->first();
                $details = new SuratTugasDetails;
                $details->surat_tugas_id = $surat->id;
                $details->perusahaan_id = $value->perusahaan_id;
                $details->perusahaan_nama = $spbu->nama_spbu;
                $details->perusahaan_kode = $spbu->kode_spbu;
                $details->nomor_nip = $value->nomor_nip;
                $details->nama_staff = $value->nama_staff;
                $details->unit_kerja = $value->unit_kerja;
                $details->jabatan = $value->jabatan;
                $details->save();

                $log = SuratTugasTemp::where('id', $value->id)->where('visible', 1)->first();
                $log->visible = 0;
                $log->save();
            }
        }

        return redirect()->route('surat-tugas.index');
    }

}
