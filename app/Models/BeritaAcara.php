<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BeritaAcara extends Model
{
    protected $table = 'berita_acara';

    public function cerapan_spbu()
    {
        return $this->hasMany(CerapanSpbu::class, 'berita_acara_id', 'id');
    }

    public function surat_tugas()
    {
        return $this->belongsTo(SuratTugas::class, 'surat_tugas_id', 'id');
    }
}
