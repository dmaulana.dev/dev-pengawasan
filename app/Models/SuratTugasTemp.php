<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratTugasTemp extends Model
{
    protected $table = 'surat_tugas_temp';

    public function spbu()
    {
        return $this->belongsTo(MSpbu::class, 'perusahaan_id', 'id');
    }

    public function minol()
    {
        return $this->belongsTo(MMinumanAlkohol::class, 'perusahaan_id', 'id');
    }

}
