<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_staff', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('profile_type');
            $table->string('nama_lengkap');
            $table->string('nomor_nip');
            $table->longText('email');
            $table->string('nomor_hp')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('unit_kerja')->nullable();
            $table->string('golongan')->nullable();
            $table->integer('active')->default('1');
            $table->longText('photo')->nullable();
            $table->longText('photo_url')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profil_staff');
    }
}
