<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratTugasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_tugas', function (Blueprint $table) {
            $table->id();
            $table->integer('perusahaan_id');
            $table->string('perusahaan_tipe');
            $table->text('nomor_surat')->nullable();
            $table->date('tanggal_surat');
            $table->string('objek_pengawasan');
            $table->date('tanggal_pelaksanaan');
            $table->string('perihal');
            $table->string('jenis_surat');
            $table->string('tentang');
            $table->text('berkas')->nullable();
            $table->text('berkas_url')->nullable();
            $table->string('kode_sarana')->nullable();
            $table->string('status');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_tugas');
    }
}
