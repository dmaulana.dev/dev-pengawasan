<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratTugasDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_tugas_details', function (Blueprint $table) {
            $table->id();
            $table->integer('surat_tugas_id');
            $table->integer('perusahaan_id');
            $table->string('perusahaan_kode');
            $table->string('perusahaan_nama')->nullable();
            $table->integer('nomor_nip');
            $table->string('nama_staff');
            $table->string('golongan');
            $table->string('nomor_hp');
            $table->string('unit_kerja');
            $table->string('jabatan');
            $table->string('status');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_tugas_details');
    }
}
