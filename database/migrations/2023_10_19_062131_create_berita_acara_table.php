<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeritaAcaraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('berita_acara', function (Blueprint $table) {
            $table->id();
            $table->integer('kode_perusahaan');
            $table->string('nama_perusahaan');
            $table->string('surat_tugas_id');
            $table->string('id_berita_acara');
            $table->string('hasil_pelaksanaan');
            $table->string('kesimpulan')->nullable();
            $table->string('jenis_berita_acara');
            $table->string('photo')->nullable();
            $table->string('photo_url')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('berita_acara');
    }
}
