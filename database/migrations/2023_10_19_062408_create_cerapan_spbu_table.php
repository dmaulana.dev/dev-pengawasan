<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCerapanSpbuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cerapan_spbu', function (Blueprint $table) {
            $table->id();
            $table->integer('berita_acara_id');
            $table->string('merk_pompa');
            $table->string('tipe');
            $table->string('nomor_seri');
            $table->string('nomor_nozel');
            $table->string('media')->nullable();
            $table->string('hasil')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cerapan_spbu');
    }
}
