<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMSpbuTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('m_spbu', function (Blueprint $table) {
            $table->id();
            $table->string('kode_spbu');
            $table->string('nama_spbu');
            $table->text('alamat');
            $table->string('nama_pengelola');
            $table->text('nomor_hp');
            $table->text('email');
            $table->text('nomor_nib')->nullable();
            $table->date('tanggal_nib')->nullable();
            $table->text('nomor_sertifikat_uttp')->nullable();
            $table->date('tanggal_sertifikat_uttp')->nullable();
            $table->date('expired_date')->nullable();
            $table->string('active')->default(1);
            $table->string('status');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('m_spbu');
    }
}
