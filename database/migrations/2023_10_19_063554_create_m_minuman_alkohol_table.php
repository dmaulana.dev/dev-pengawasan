<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMMinumanAlkoholTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_minuman_alkohol', function (Blueprint $table) {
            $table->id();
            $table->string('kode');
            $table->string('nama');
            $table->string('alamat');
            $table->string('nama_pengelola');
            $table->integer('nomor_hp');
            $table->integer('email');
            $table->integer('nomor_nib')->nullable();
            $table->string('tanggal_nib')->nullable();
            $table->integer('nomor_sertifikat_uttp')->nullable();
            $table->date('tanggal_sertifikat_uttp')->nullable();
            $table->date('expired_date')->nullable();
            $table->string('status');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_minuman_alkohol');
    }
}
