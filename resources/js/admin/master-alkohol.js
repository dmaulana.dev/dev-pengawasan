$(function () {
    var table = $('.datatables-alkohol').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: `/master-minuman-alkohol`,
        },
        columns: [
            { data: "" },
            { data: "kode", name: "kode"},
            { data: "nama", name: "nama"},
            { data: "nama_pengelola", name: "nama_pengelola" },
            { data: "nomor_hp", name: "nomor_hp" },
            { data: "nomor_nib", name: "nomor_nib" },
            { data: "nomor_sertifikat_uttp", name: "nomor_sertifikat_uttp" },
            { data: "status", name: "status" },
            { data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        columnDefs: [
            {
                className: "center",
                orderable: true,
                searchable: false,
                responsivePriority: 2,
                targets: 0,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
            },
            {
                targets: 7,
                render: function (data, type, full, meta) {
                    var status = full['status'];
                    if (status == 'perpanjang') {
                        return '<span class="badge rounded-pill bg-label-success">' + status + '</span>';
                    }else if(status == 'expired'){
                        return '<span class="badge rounded-pill bg-label-secondary">' + status + '</span>';
                    }else{
                        return '<span class="badge rounded-pill bg-label-danger">' + status + '</span>';
                    }
                }

            }
        ],
        order: [[0, "asc"]],
    });

    $('body').on('click', '.editSpbu', function () {
        var id = $(this).data('id');
        $.ajax({
            url: `/admin/spbu-show/${id}`,
            type: "GET",
            cache: false,
            success:function(response){
                let data = response.data;

                //fill data to form
                $('#spbu_id').val(data.id);
                $('#nama_spbu').val(data.nama_spbu);
                $('#kode_spbu').val(data.kode_spbu);
                $('#nama_pengelola').val(data.nama_pengelola);
                $('#nomor_hp').val(data.nomor_hp);
                $('#nomor_nib').val(data.nomor_nib);
                $('#nomor_sertifikat_uttp').val(data.nomor_sertifikat_uttp);
                $('#alamat').val(data.alamat);
                $('#email').val(data.email);

                //open modal
                $('#editSpbuModal').modal('show');
            }
        });
    });

});
