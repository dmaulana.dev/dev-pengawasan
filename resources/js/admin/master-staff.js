$(function () {
    var table = $('.datatables-staff').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: `/master-staff`,
        },
        columns: [
            { data: "" },
            { data: "nomor_nip", name: "nomor_nip"},
            { data: "nama_lengkap", name: "nama_lengkap"},
            { data: "email", name: "email" },
            { data: "nomor_hp", name: "nomor_hp" },
            { data: "jabatan", name: "jabatan" },
            { data: "unit_kerja", name: "unit_kerja" },
            // { data: "golongan", name: "golongan" },
            { data: "active", name: "active" },
            { data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        columnDefs: [
            {
                targets: 7,
                render: function (data, type, full, meta) {
                    var $status_number = full['active'];
                    var $status = {
                        0: { title: 'Tidak Aktif', class: ' bg-label-danger' },
                        1: { title: 'Aktif', class: ' bg-label-success' },
                    };

                    return (
                        '<span class="badge rounded-pill ' +
                        $status[$status_number].class +
                        '">' +
                        $status[$status_number].title +
                        '</span>'
                        );
                    }
                },
                {
                    className: "center",
                    orderable: true,
                    searchable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
            ],
            dom:
            '<"row mx-2"' +
            '<"col-md-2"<"me-3"l>>' +
            '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
            '>t' +
            '<"row mx-2"' +
            '<"col-sm-12 col-md-6"i>' +
            '<"col-sm-12 col-md-6"p>' +
            '>',
            language: {
                sLengthMenu: 'Show _MENU_',
                search: '',
                searchPlaceholder: 'Search..'
            },
            order: [[0, "asc"]],
            buttons: [
                {
                    extend: 'collection',
                    className: 'btn btn-label-secondary dropdown-toggle mx-3',
                    text: '<i class="mdi mdi-export-variant me-1"></i> <span class="d-none d-sm-inline-block">Export</span>',
                    buttons: [
                        {
                            extend: 'excel',
                            text: '<i class="mdi mdi-file-excel-outline me-1"></i>Excel',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: [1, 2, 3, 4, 5],
                                // prevent avatar to be display
                                format: {
                                    body: function (inner, coldex, rowdex) {
                                        if (inner.length <= 0) return inner;
                                        var el = $.parseHTML(inner);
                                        var result = '';
                                        $.each(el, function (index, item) {
                                            if (item.classList !== undefined && item.classList.contains('user-name')) {
                                                result = result + item.lastChild.firstChild.textContent;
                                            } else if (item.innerText === undefined) {
                                                result = result + item.textContent;
                                            } else result = result + item.innerText;
                                        });
                                        return result;
                                    }
                                }
                            }
                        },
                    ]
                },
                {
                    text: '<i class="mdi mdi-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block">TAMBAH STAFF</span>',
                    className: 'add-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#addStaffModal'
                    },
                }
            ],
        });

    });
