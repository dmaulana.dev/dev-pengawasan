$(function () {
    var table = $('.datatables-spbu').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: `/surat-tugas/get-spbu`,
        },
        columns: [
            { data: "" },
            { data: "kode_spbu", name: "kode_spbu"},
            { data: "nama_spbu", name: "nama_spbu"},
            { data: "nama_pengelola", name: "nama_pengelola" },
            { data: "nomor_hp", name: "nomor_hp" },
            { data: "nomor_sertifikat_uttp", name: "nomor_sertifikat_uttp" },
            { data: "status", name: "status" },
            { data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        columnDefs: [
            {
                targets: -2,
                render: function (data, type, full, meta) {
                    var status = full['status'];
                    if (status == 'perpanjang') {
                        return '<span class="badge rounded-pill bg-label-success">' + status + '</span>';
                    }else if(status == 'expired'){
                        return '<span class="badge rounded-pill bg-label-secondary">' + status + '</span>';
                    }else{
                        return '<span class="badge rounded-pill bg-label-danger">' + status + '</span>';
                    }
                }

            },
            {
                className: "center",
                orderable: true,
                searchable: false,
                responsivePriority: 2,
                targets: 0,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
            },

        ],
        order: [[0, "asc"]],
    });

    $('body').on('click', '.addStaff', function () {
        var id = $(this).data('id');
        $.ajax({
            url: `/master-spbu/show/${id}`,
            type: "GET",
            cache: false,
            success:function(response){
                let data = response.data;

                //fill data to form
                $('#perusahaan_id').val(data.id);

                //open modal
                $('#modalPetugasAdd').modal('show');
            }
        });
    });

    $('body').on('click', '.detailsStaff', function () {
        var id = $(this).data('id');
        $('#modalDetailsStaff').modal('show');
        var table = $('.datatables-temp-details').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: `/surat-tugas/temp-details/${id}`,
            },
            columns: [
                { data: "" },
                { data: "nomor_nip", name: "nomor_nip"},
                { data: "nama_staff", name: "nama_staff"},
                { data: "unit_kerja", name: "unit_kerja" },
                { data: "jabatan", name: "jabatan" },
                { data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            columnDefs: [
                {
                    className: "center",
                    orderable: true,
                    searchable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
            ],
            order: [[0, "asc"]],
        });

    });


    $('#modalDetailsStaff').on('hidden.bs.modal', function (e) {
        location.reload('#modalDetailsStaff');
    })

});
