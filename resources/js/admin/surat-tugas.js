$(function () {
    var table = $('.datatables-surat-tugas').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: `/surat-tugas`,
        },
        columns: [
            { data: "" },
            { data: "tanggal_surat", name: "tanggal_surat" },
            { data: "tanggal_pelaksanaan", name: "tanggal_pelaksanaan" },
            { data: "perusahaan_kode", name: "perusahaan_kode" },
            { data: "perusahaan_nama", name: "perusahaan_nama" },
            { data: "status", name: "status" },
            { data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        columnDefs: [
            {
                className: "center",
                orderable: true,
                searchable: false,
                responsivePriority: 2,
                targets: 0,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
            },

            {
                targets: 5,
                render: function (data, type, full, meta) {
                    var status = full['status'];
                    if (status == 'penugasan') {
                        return '<span class="badge rounded-pill bg-label-info">' + status + '</span>';
                    }else if(status == 'pengawasan'){
                        return '<span class="badge rounded-pill bg-label-warning">' + status + '</span>';
                    }else if(status == 'perpanjang'){
                        return '<span class="badge rounded-pill bg-label-success">' + status + '</span>';
                    }else{
                        return '<span class="badge rounded-pill bg-label-secondary">' + status + '</span>';
                    }
                }

            }
        ],
        dom:
        '<"row mx-2"' +
        '<"col-md-2"<"me-3"l>>' +
        '<"col-md-10"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-end flex-md-row flex-column mb-3 mb-md-0"fB>>' +
        '>t' +
        '<"row mx-2"' +
        '<"col-sm-12 col-md-6"i>' +
        '<"col-sm-12 col-md-6"p>' +
        '>',
        language: {
            sLengthMenu: 'Show _MENU_',
            search: '',
            searchPlaceholder: 'Search..'
        },
        order: [[0, "asc"]],
        buttons: [
            {
                extend: 'collection',
                className: 'btn btn-label-secondary dropdown-toggle mx-3',
                text: '<i class="mdi mdi-export-variant me-1"></i> <span class="d-none d-sm-inline-block">Export</span>',
                buttons: [
                    {
                        extend: 'excel',
                        text: '<i class="mdi mdi-file-excel-outline me-1"></i>Excel',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5],
                            // prevent avatar to be display
                            format: {
                                body: function (inner, coldex, rowdex) {
                                    if (inner.length <= 0) return inner;
                                    var el = $.parseHTML(inner);
                                    var result = '';
                                    $.each(el, function (index, item) {
                                        if (item.classList !== undefined && item.classList.contains('user-name')) {
                                            result = result + item.lastChild.firstChild.textContent;
                                        } else if (item.innerText === undefined) {
                                            result = result + item.textContent;
                                        } else result = result + item.innerText;
                                    });
                                    return result;
                                }
                            }
                        }
                    }
                ]
            },
            {
                text: '<i class="mdi mdi-plus me-0 me-sm-1"></i><span class="d-none d-sm-inline-block">SURAT TUGAS</span>',
                className: 'add-new btn btn-primary add-surat-tugas',
                action: function (e, dt, button, config) {
                    window.location = 'surat-tugas/create';
                }
            }
        ],
    });


    $('body').on('click', '.detailsSurat', function () {
        var id = $(this).data('id');
        $('#modalDetailsSuratTugas').modal('show');
        var table = $('.datatables-details-surat-spbu').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: `/admin/surat-tugas/details/${id}`,
            },
            columns: [
                { data: "" },
                { data: "nomor_nip", name: "nomor_nip"},
                { data: "nama_staff", name: "nama_staff"},
                { data: "unit_kerja", name: "unit_kerja" },
                { data: "jabatan", name: "jabatan" },
                { data: "no_hp", name: "no_hp" },
            ],
            columnDefs: [
                {
                    className: "center",
                    orderable: true,
                    searchable: false,
                    responsivePriority: 2,
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
            ],
            order: [[0, "asc"]],
        });
    });

    // $('#modalDetailsSuratTugas').on('hidden.bs.modal', function (e) {
    //     location.reload('#modalDetailsSuratTugas');
    // });

    $('body').on('click', '.addCerapanSpbu', function () {
        var id = $(this).data('id');
        $.ajax({
            url: `/admin/surat-tugas/show/${id}`,
            type: "GET",
            cache: false,
            success:function(response){
                let data = response.data;

                //fill data to form
                $('#surat_id').val(data.id);
                $('#no_surat').val(data.nomor_surat);
                $('#tanggal_surat').val(data.tanggal_surat);
                $('#tanggal_pelaksanaan').val(data.tanggal_pelaksanaan);
                $('#spbu_id').val(data.m_spbu_id);
                $('#kode_spbu').val(data.spbu.kode_spbu);
                $('#nama_spbu').val(data.spbu.nama_spbu);
                $('#no_uttp').val(data.spbu.nomor_sertifikat_uttp);
                $('#pengelola').val(data.spbu.nama_pengelola);
                $('#alamat').val(data.spbu.alamat);

                //open modal
                $('#modalAddCerapanSpbu').modal('show');
            }
        });
    });

    $('#modalAddCerapanSpbu').on('hidden.bs.modal', function (e) {
        location.reload('#modalAddCerapanSpbu');
    });

});
