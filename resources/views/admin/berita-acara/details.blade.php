@extends('layouts.admin._master-admin')
@section('content')

<!-- Content -->
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4">
        <button type="button" onclick="history.back()" class="btn btn-text-dark btn-sm">
            <span class="tf-icons mdi mdi-skip-previous me-2"></span>Kembali
        </button>
        <span class="text-muted fw-light">Berita Acara /</span> Details</h4>

    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <!-- Current Plan -->
                <h5 class="card-header">Informasi Perusahaan</h5>
                <div class="card-body pt-1">
                    <div class="row">
                        <div class="col-md-6 mb-1">
                            <div class="mb-4">
                                <h6 class="mb-1 fw-semibold">Nama</h6>
                                <span>{{$perusahaan->nama_spbu}}</span>
                                <br/>
                                <span class="badge bg-label-primary rounded-pill">{{$perusahaan->kode_spbu}}</span>
                            </div>
                            <div class="mb-4">
                                <h6 class="mb-1 fw-semibold">Pengelola / Pemilik</h6>
                                <span>{{$perusahaan->nama_pengelola}}</span>
                                <br/>
                                <span class="badge bg-label-primary rounded-pill">{{$perusahaan->no_hp}}</span>
                            </div>
                            <div>
                                <h6 class="mb-1 fw-semibold">
                                    <span class="me-2">Alamat</span>
                                    {{-- <span class="badge bg-label-primary rounded-pill">Popular</span> --}}
                                </h6>
                                <p class="mb-0">{{$perusahaan->alamat}}</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            @if ($status == 'expired')
                            <div class="alert alert-secondary mb-4 alert-dismissible" role="alert">
                                <h6 class="alert-heading mb-1 d-flex align-items-end">
                                    <i class="mdi mdi-alert-outline mdi-20px me-2"></i>
                                    <span>Status Perusahaan Saat ini!</span>
                                </h6>
                                <span class="ms-4 ps-1"><b>{{$status}}</b> {{$after_date}} hari yang lalu</span>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="close"></button>
                            </div>
                            @elseif ($status == 'perpanjang')
                            <div class="alert alert-success mb-4 alert-dismissible" role="alert">
                                <h6 class="alert-heading mb-1 d-flex align-items-end">
                                    <i class="mdi mdi-alert-outline mdi-20px me-2"></i>
                                    <span>Status Perusahaan Saat ini!</span>
                                </h6>
                                <span class="ms-4 ps-1"><b>{{$status}}<b> {{$after_date}} hari lagi</span>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="close"></button>
                            </div>
                            @else
                            <div class="alert alert-danger mb-4 alert-dismissible" role="alert">
                                <h6 class="alert-heading mb-1 d-flex align-items-end">
                                    <i class="mdi mdi-alert-outline mdi-20px me-2"></i>
                                    <span>Status Perusahaan Saat ini!</span>
                                </h6>
                                <span class="ms-4 ps-1"><b>{{$status}}</b></span>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="close"></button>
                            </div>
                            @endif
                            <div class="mb-4">
                                <h6 class="mb-1 fw-semibold">
                                    <span class="me-2">Nomor NIB</span>
                                </h6>
                                <span class="mb-0">{{$perusahaan->nomor_nib}}</span>
                                <span>-</span>
                                <span class="badge bg-label-primary rounded-pill">{{$perusahaan->tanggal_nib}}</span>
                            </div>
                            <div>
                                <h6 class="mb-1 fw-semibold">
                                    <span class="me-2">No Sertifikat UTTP</span>
                                </h6>
                                <span class="mb-0">{{$perusahaan->nomor_sertifikat_uttp}}</span>
                                <span>-</span>
                                <span class="badge bg-label-primary rounded-pill">{{$perusahaan->tanggal_sertifikat_uttp}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <h5 class="card-header">Informasi Surat Tugas</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row g-4">
                                <div class="col-12">
                                    <h6 class="mb-1 fw-semibold">
                                        <span class="badge bg-label-primary rounded-pill">
                                            <span class="mdi mdi-download-circle"></span>
                                            Dokumen Surat Tugas
                                        </span>
                                    </h6>
                                </div>

                                <div class="col-12">
                                    <h6 class="mb-1 fw-semibold">
                                        <span class="me-2">Nomor Surat</span>
                                    </h6>
                                    <span class="mb-0">{{$surat->nomor_surat}}</span>
                                </div>
                                <div class="col-12 col-md-3">
                                    <h6 class="mb-1 fw-semibold">
                                        <span class="me-2">Tanggal Surat</span>
                                    </h6>
                                    <span class="mb-0">{{$surat->tanggal_surat}}</span>
                                </div>
                                <div class="col-12 col-md-3">
                                    <h6 class="mb-1 fw-semibold">
                                        <span class="me-2">Perihal</span>
                                    </h6>
                                    <span class="mb-0">{{$surat->perihal}}</span>
                                </div>
                                <div class="col-12 col-md-3">
                                    <h6 class="mb-1 fw-semibold">
                                        <span class="me-2">Tentang</span>
                                    </h6>
                                    <span class="mb-0">{{$surat->tentang}}</span>
                                </div>
                                <div class="col-12">
                                    <h6 class="mb-1 fw-semibold">
                                        <span class="me-2">Tanggal Pelaksanaan</span>
                                    </h6>
                                    <span class="mb-0">{{$surat->tanggal_pelaksanaan}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mt-5 mt-md-0">
                            <h6>Staff / Petugas Lapangan</h6>
                            @foreach ($surat->TSuratTugasDetails as $row)
                            <div class="added-cards">
                                <div class="cardMaster bg-lighter p-3 rounded mb-3">
                                    <div class="d-flex justify-content-between flex-sm-row flex-column">
                                        <div class="card-information me-2">
                                            <div class="d-flex align-items-center mb-1 flex-wrap gap-2">
                                                <h6 class="mb-0 me-2 fw-semibold">{{$row->nama_staff}}</h6>
                                                <span class="badge bg-label-primary rounded-pill">{{$row->nomor_nip}}</span>
                                            </div>
                                            <span class="card-number">{{$row->no_hp}}</span>
                                        </div>
                                        <div class="d-flex flex-column text-start text-lg-end">
                                            <small class="mt-sm-auto mt-2 order-sm-1 order-0 text-muted">
                                                {{$row->unit_kerja}} - {{$row->jabatan}}
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <h5 class="card-header">Informasi Berita Acara</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row g-4">
                                <div class="col-12">
                                    <h6 class="mb-1 fw-semibold">
                                        <a href="{{ route('admin.berita-acara.pdf', $berita->id) }}" target="_blank" class="badge bg-label-primary rounded-pill">
                                            <span class="mdi mdi-download-circle"></span>
                                            Dokumen Berita Acara
                                        </a>
                                    </h6>
                                </div>

                                <div class="col-12 6">
                                    <h6 class="mb-1 fw-semibold">
                                        <span class="me-2">Hasil Pelaksanaan</span>
                                    </h6>
                                    <span class="mb-0">{{$berita->hasil_pelaksanaan}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mt-5 mt-md-0">
                            <div class="col-12">
                                <h6 class="mb-1 mt-5 fw-semibold">
                                    <span class="me-2">Kesimpulan Lanjut / Tidak</span>
                                </h6>
                                <span class="mb-0">{{$berita->kesimpulan}}</span>
                            </div>
                        </div>

                        <div class="col-md-12 mt-5 mt-md-0">
                            <div class="col-12">
                                <h6 class="mb-1 mt-5 fw-semibold">
                                    <span class="me-2">Cerapan SPBU</span>
                                </h6>
                                <br/>
                                <table class="table m-0">
                                    <thead class="table-light border-top">
                                        <tr>
                                            <th></th>
                                            <th>Merk Pompa</th>
                                            <th>Tipe</th>
                                            {{-- <th><i class="mdi mdi-trending-up"></i></th> --}}
                                            <th>No Seri</th>
                                            <th>No Nozwl</th>
                                            {{-- <th class="text-truncate">Issued Date</th> --}}
                                            <th>Media</th>
                                            <th>Hasil</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($cerapan as $key => $item)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$item->merk_pompa}}</td>
                                            <td>{{$item->tipe}}</td>
                                            <td>{{$item->nomor_seri}}</td>
                                            <td>{{$item->nomor_nozel}}</td>
                                            <td>{{$item->media}}</td>
                                            <td>{{$item->hasil}}</td>
                                            <td>{{$item->keterangan}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

</div>
<!-- Content -->
@push('custom-scripts')
<script src="{{ URL::asset('resources/js/admin/spbu-cerapan.js') }}"></script>
@endpush

@endsection
