@extends('layouts.admin._master-admin')
@section('content')

<!-- Content -->
<div class="container-xxl flex-grow-1 container-p-y">

    {{-- <h4 class="fw-bold py-3"><span class="text-muted fw-light">Master /</span> Surat Tugas </h4> --}}
    <div class="row g-4 mb-4">
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="avatar">
                            <div class="avatar-initial bg-label-primary rounded">
                                <div class="mdi mdi-account-outline mdi-24px"></div>
                            </div>
                        </div>
                        <div class="ms-3">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-0">8,458</h5>
                                <div class="mdi mdi-chevron-down text-danger mdi-24px"></div>
                                <small class="text-danger">8.1%</small>
                            </div>
                            <small class="text-muted">New Customers</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="avatar">
                            <div class="avatar-initial bg-label-warning rounded">
                                <div class="mdi mdi-poll mdi-24px"></div>
                            </div>
                        </div>
                        <div class="ms-3">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-0">$28.5K</h5>
                                <div class="mdi mdi-chevron-up text-success mdi-24px"></div>
                                <small class="text-success">18.2%</small>
                            </div>
                            <small class="text-muted">Total Profit</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="avatar">
                            <div class="avatar-initial bg-label-info rounded">
                                <div class="mdi mdi-trending-up mdi-24px"></div>
                            </div>
                        </div>
                        <div class="ms-3">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-0">2,450K</h5>
                                <div class="mdi mdi-chevron-down text-danger mdi-24px"></div>
                                <small class="text-danger">24.6%</small>
                            </div>
                            <small class="text-muted">New Transaction</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="avatar">
                            <div class="avatar-initial bg-label-success rounded">
                                <div class="mdi mdi-currency-usd mdi-24px"></div>
                            </div>
                        </div>
                        <div class="ms-3">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-0">$48.2K</h5>
                                <div class="mdi mdi-chevron-down text-success mdi-24px"></div>
                                <small class="text-success">22.5%</small>
                            </div>
                            <small class="text-muted">Total Revenue</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- DataTable with Buttons -->
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Data Berita Acara</h5>
        </div>
        <div class="card-datatable table-responsive pt-0">
            <table class="datatables-berita-acara table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No Surat</th>
                        <th>Kode</th>
                        <th>Perusahaan</th>
                        <th>Hasil Pelaksanaan</th>
                        <th>Kesimpulan</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!--/ DataTable with Buttons -->

    <!-- Create App Modal -->
    <div class="modal fade" id="modalDetailsSuratTugas" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered modal-simple modal-upgrade-plan">
            <div class="modal-content p-3 p-md-5">
                <div class="modal-body p-1">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center">
                        <h3 class="mb-2 pb-1">Details Surat Tugas SPBU</h3>
                        <p id="spbu"></p>
                    </div>
                    <div class="card-datatable table-responsive pt-0">
                        <table class="datatables-berita-acara table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nomor NIP</th>
                                    <th>Nama Staff</th>
                                    <th>Unit Kerja</th>
                                    <th>Jabatan</th>
                                    <th>No HP</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ Create App Modal -->


</div>
<!-- Content -->
@push('custom-scripts')
{{-- <script src="{{ URL::asset('resources/js/master/surat-tugas-datatable.js') }}"></script> --}}
<script src="{{ URL::asset('resources/js/admin/berita-acara-datatable.js') }}"></script>
{{-- <script src="{{ URL::asset('public/admin/assets/js/modal-create-app.js') }}"></script> --}}
@endpush

@endsection
