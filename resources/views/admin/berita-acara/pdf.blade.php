<!DOCTYPE html>

<html>
<head>

    @php
    $year = Carbon\Carbon::now()->format('Y');
    $date = Carbon\Carbon::now()->format('d');
    $dateM = Carbon\Carbon::now()->format('m');
    $days = Carbon\Carbon::now()->format('l');
    $month = Carbon\Carbon::now()->format('F');
    @endphp
    <body>
        <div>
            <p style="text-align: center;">
                <b>BERITA ACARA HASIL PENGAWASAN</b>
                <br/>
            </p>
        </div>
        <table style="width: 100%;">
            <tr>
                <td>
                    <p align="justify">
                        Pada hari ini, {{ $days }} tanggal {{ $date }} bulan {{$month }} tahun {{ $year }},
                        sesuai Surat Tugas dari Kepala Dinas Perindustrian, Perdagangan, Koperasi, Usaha Kecil dan Menengah Provinsi DKI Jakarta Nomor : {{$berita_acara->nomor_surat}} tanggal {{$berita_acara->surat_tugas->tgl_pelaksanaan}},
                        telah melakukan Pengawasan di wilayah Provinsi DKI Jakarta dengan hasil sebagai berikut :
                    </p>
                </td>
            </tr>
        </table>
        <table style="width: 95%">
            <tr>
                <td>1. Nama Perusahaan</td>
                <td>:</td>
                <td>
                    {{$perusahaan->nama_spbu}}
                </td>
            </tr>
            <tr>
                <td>2. Penanggung Jawab</td>
                <td>:</td>
                <td>
                    {{$perusahaan->nama_pengelola}}
                </td>
            </tr>
            <tr>
                <td>3. Alamat Perusahaan</td>
                <td>:</td>
                <td>
                    {{$perusahaan->alamat}}
                </td>
            </tr>
            <tr>
                <td>4. Ijin Usaha</td>
                <td>:</td>
                <td>
                    {{$perusahaan->nomor_nib}}
                </td>
            </tr>
            <tr>
                <td>5. Hasil Pengawasan **</td>
                <td>:</td>
                <td>
                    {{ $berita_acara->hasil_pelaksanaan }}
                </td>
            </tr>
            <tr>
                <td>6. Kesimpulan / Tindak Lanjut</td>
                <td>:</td>
                <td>
                    {{ $berita_acara->kesimpulan}}
                </td>
            </tr>
        </table>
        <p align="justify">Demikian Berita Acara ini dibuat untuk dipergunakan seperlunya.</p>

        <div style="width: 40%; float: left; text-align: center;">
            <p>Mengetahui : </p>
            <span>Pihak Perusahaan</span>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <p class="">{{ $perusahaan->nama_pengelola}}</p>
        </div>

        <div style="width: 60%; float: left; text-align: left; padding-left:10%;">
            <p>Petugas : </p>
            @foreach ($staff as $row)
            <table>
                <tr>
                    <td>1. {{$row->nama_lengkap}}</td>
                    <td>:</td>
                    <td>{{$row->nomor_nip}} {{$row->jabatan}} {{$row->unit_kerja}}</td>
                </tr>
            </table>
            @endforeach
        </div>

        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>

        <div style="padding-top: 330px;">
            <p>**
                Untuk melengkapi kekurangan hasil pengawasan data / dokumen
                dapat dikirimkan ke email :
                <a href="">bidangpengawasan.ppkukm@gmail.com</a> dengan subjek :
                kelengkapan dokumen pengawasan "-nama lokasi usaha-"
            </p>

            <div style="page-break-after: always;"></div>

            <p style="text-align: center;">
                <b>CERAPAN PENGAWASAN KEMETROLOGIAN</b>
                <br/>
            </p>

            <table style="width: 95%">
                <tr>
                    <td>Nama Perusahaan</td>
                    <td>:</td>
                    <td>
                        {{$perusahaan->nama_spbu}}
                    </td>
                </tr>
                <tr>
                    <td>Penanggung Jawab</td>
                    <td>:</td>
                    <td>{{$perusahaan->nama_pengelola}}</td>
                </tr>
                <tr>
                    <td>Alamat Perusahaan</td>
                    <td>:</td>
                    <td>
                        {{$perusahaan->alamat}}
                    </td>
                </tr>
                <tr>
                    <td>Nomor Telepon</td>
                    <td>:</td>
                    <td>{{$perusahaan->no_hp}}</td>
                </tr>
            </table>
            <br/>
            <table style="padding-top: 10px; padding-bottom: 10px; text-align: center; width: 100%; border: 1px solid black; border-collapse: collapse;">
                <tr>
                    <th rowspan="2" style="width: 5%;  padding:5px; border: 1px solid black; border-collapse: collapse;";><b>No</b></th>
                    <th rowspan="2" style="width: 25%; padding:5px; border: 1px solid black; border-collapse: collapse;";><b>Merk Pompa</b></th>
                    <th rowspan="2" style="width: 25%; padding:5px; border: 1px solid black; border-collapse: collapse;";><b>Tipe</b></th>
                    <th rowspan="2" style="width: 25%; padding:5px; border: 1px solid black; border-collapse: collapse;";><b>No Seri</b></th>
                    <th rowspan="2" style="width: 25%; padding:5px; border: 1px solid black; border-collapse: collapse;";><b>No Nozel</b></th>
                    <th rowspan="2" style="width: 25%; padding:5px; border: 1px solid black; border-collapse: collapse;";><b>Media</b></th>
                    <th colspan="3" style="width: 25%; padding:5px; border: 1px solid black; border-collapse: collapse;";>
                        <b>Penunjukan Per 20 Liter</b>
                    </th>
                    <th rowspan="2"  style="width: 25%; padding:5px; border: 1px solid black; border-collapse: collapse;";><b>Keterangan</b></th>

                </tr>
                <tr>
                    <th style="width: 25%; padding:5px; border: 1px solid black; border-collapse: collapse;";>
                        <b>Lambat</b>
                    </th>
                    <th style="width: 25%; padding:5px; border: 1px solid black; border-collapse: collapse;";>
                        <b>Sedang</b>
                    </th>
                    <th style="width: 25%; padding:5px; border: 1px solid black; border-collapse: collapse;";>
                        <b>Cepat</b>
                    </th>
                </tr>
                @foreach ($berita_acara->cerapan_spbu as $key => $value)
                <tr>
                    <td style="padding:5px; border: 1px solid black; border-collapse: collapse;";> {{ $key + 1 }} </td>
                    <td style="padding:5px; border: 1px solid black; border-collapse: collapse;";> {{ $value->merk_pompa }} </td>
                    <td style="padding:5px; border: 1px solid black; border-collapse: collapse;";> {{ $value->tipe }} </td>
                    <td style="padding:5px; border: 1px solid black; border-collapse: collapse;";> {{ $value->nomor_seri }} </td>
                    <td style="padding:5px; border: 1px solid black; border-collapse: collapse;";> {{ $value->nomor_nozel }} </td>
                    <td style="padding:5px; border: 1px solid black; border-collapse: collapse;";> {{ $value->media }} </td>
                    <td style="padding:5px; border: 1px solid black; border-collapse: collapse;";> {{ $value->hasil == 'Lambat' ? $value->hasil : ''}} </td>
                    <td style="padding:5px; border: 1px solid black; border-collapse: collapse;";> {{ $value->hasil == 'Sedang' ? $value->hasil : ''}} </td>
                    <td style="padding:5px; border: 1px solid black; border-collapse: collapse;";> {{ $value->hasil == 'Cepat' ? $value->hasil : ''}} </td>
                    <td style="padding:5px; border: 1px solid black; border-collapse: collapse;";> {{ $value->keterangan }} </td>
                </tr>
                @endforeach
            </table>
            <div style="width: 40%; float: left; text-align: center;">
                <p>Penanggung Jawab</p>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <p class="">{{ $perusahaan->nama_pengelola}}</p>
            </div>

            <div style="width: 60%; float: left; text-align: left; padding-left:10%;">
                <p>Petugas : </p>
                @foreach ($staff as $row)
                <table>
                    <tr>
                        <td>1. {{$row->nama_lengkap}}</td>
                        <td>:</td>
                        <td>{{$row->nomor_nip}} {{$row->jabatan}} {{$row->unit_kerja}}</td>
                    </tr>
                </table>
                @endforeach
            </div>



        </body>
    </head>
    </html>
