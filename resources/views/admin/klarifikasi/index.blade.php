@extends('layouts.admin._master-admin')
@section('content')

<div class="container-xxl flex-grow-1 container-p-y">

    <!-- Content -->
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Data Klarifikasi</h5>
        </div>
        <div class="card-datatable table-responsive pt-0">
            <table class="datatables-klarifikasi table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No Surat</th>
                        <th>Kode</th>
                        <th>Perusahaan</th>
                        <th>Hasil Pelaksanaan</th>
                        <th>Kesimpulan</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!-- Content -->

</div>

@push('custom-scripts')
<script src="{{ URL::asset('resources/js/admin/klarifikasi-datatable.js') }}"></script>
@endpush

@endsection
