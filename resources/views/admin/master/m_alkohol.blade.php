@extends('layouts.admin._master-admin')
@section('content')

<!-- Content -->
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Master /</span> Perusahaan Minuman Alkohol </h4>

    <!-- DataTable with Buttons -->
    <div class="card">
        <div class="card-datatable table-responsive pt-0">
            <table class="datatables-alkohol table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Pengelola</th>
                        <th>No HP</th>
                        <th>NIB</th>
                        <th>No. UTTP</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!--/ DataTable with Buttons -->

    <!-- Edit User Modal -->
    <div class="modal fade" id="editSpbuModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-simple modal-edit-user">
            <div class="modal-content p-3 p-md-5">
                <div class="modal-body py-3 py-md-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center mb-4">
                        <h3 class="mb-2">Form Ubah Data Minuman Alkohol</h3>
                        <p class="pt-1">Silahkan lengkapi kebutuhan data dibawah ini.</p>
                    </div>
                    <form action="{{ route('master-spbu.update')}}" class="row g-4" method="POST">
                        @csrf
                        <input type="hidden" id="spbu_id" name="spbu_id" class="form-control" value="" />

                        <div class="col-12 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="nama_spbu" name="nama_spbu"
                                class="form-control" value="" />
                                <label for="nama_spbu">Nama Perusahaan</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="kode_spbu" name="kode_spbu"
                                class="form-control" value="" readonly/>
                                <label for="kode_spbu">Nomor SPBU</label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="nama_pengelola" name="nama_pengelola"
                                class="form-control" value="" />
                                <label for="nama_pengelola">Nama Pengelola</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="input-group input-group-merge">
                                <span class="input-group-text">ID (+62)</span>
                                <div class="form-floating form-floating-outline">
                                    <input type="text" id="nomor_hp" name="nomor_hp"
                                    class="form-control phone-number-mask" value="" />
                                    <label for="nomor_hp">Nomor Handphone</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="email" name="email"
                                class="form-control" value="" />
                                <label for="email">Email</label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="alamat" name="alamat"
                                class="form-control" value="" />
                                <label for="alamat">Alamat Lengkap</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="nomor_nib" name="nomor_nib"
                                class="form-control" value="" />
                                <label for="nomor_nib">Nomor NIB</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="nomor_sertifikat_uttp" name="nomor_sertifikat_uttp"
                                class="form-control" placeholder="992123" />
                                <label for="nomor_sertifikat_uttp">Nomor UTTP</label>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Batal
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/ Edit User Modal -->

</div>

@push('custom-scripts')
<script src="{{ URL::asset('resources/js/admin/master-alkohol.js') }}"></script>
@endpush

@endsection
