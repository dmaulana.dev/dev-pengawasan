@extends('layouts.admin._master-admin')
@section('content')

<!-- Content -->
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Master /</span> Staff - Pertugas Lapangan</h4>

    <!-- DataTable with Buttons -->
    <div class="card">
        <div class="card-datatable table-responsive pt-0">
            <table class="datatables-staff table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nomor NIP</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>No HP</th>
                        <th>Jabatan</th>
                        <th>Unit Kerja</th>
                        {{-- <th>Golongan</th> --}}
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!--/ DataTable with Buttons -->

    <div class="modal fade" id="addStaffModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content p-3 p-md-5">
                <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal"
                aria-label="Close"></button>
                <div class="modal-body p-md-0">
                    <div class="text-center mb-4">
                        <h3 class="mb-2 pb-1">Tambah Staff</h3>
                        <p>Tambah Staff</p>
                    </div>
                    <form action="{{route('master-staff.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-12 mb-3">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="nama_lengkap" name="nama_lengkap"
                                class="form-control" placeholder="Nama Lengkap"/>
                                <label>Nama Lengkap</label>
                            </div>
                        </div>

                        <div class="form-floating form-floating-outline mt-3">
                            <input type="text" id="nomor_nip" name="nomor_nip" class="form-control" placeholder="326xxxx"/>
                            <label>Nomor NIP</label>
                        </div>

                        <div class="form-floating form-floating-outline mt-3">
                            <input type="email" id="email" name="email" class="form-control" placeholder="lasernarindo@gmail.com"/>
                            <label>Email</label>
                        </div>

                        <div class="form-floating form-floating-outline mt-3">
                            <input type="text" id="nomor_hp" name="nomor_hp" class="form-control" placeholder="081387xx26xx"/>
                            <label>No Handphone</label>
                        </div>

                        <div class="form-floating form-floating-outline mt-3">
                            <input type="text" id="jabatan" name="jabatan" class="form-control" placeholder="staff ..."/>
                            <label>Jabatan</label>
                        </div>

                        <div class="form-floating form-floating-outline mt-3">
                            <input type="text" id="unit_kerja" name="unit_kerja" class="form-control" placeholder="Pengawasan ..."/>
                            <label>Unit Kerja</label>
                        </div>

                        <div class="form-floating form-floating-outline mt-3">
                            <select class="form-control" id="role" name="profile_tipe">
                                <option value="staff">staff</option>
                                <option value="subkoor">subkoor</option>
                            </select>
                            <label>Tipe Profile</label>
                        </div>

                        <div class="form-floating form-floating-outline mt-3">
                            <select class="form-control" id="role" name="role">
                                @foreach ($list_role as $item)
                                <option value="{{ $item->id }}">{{ $item->name}}</option>
                                @endforeach
                            </select>
                            <label>Role Name</label>
                        </div>

                        <div class="col-12 text-center demo-vertical-spacing">
                            <button type="submit" class="btn btn-primary btn-submit me-sm-3 me-1">Simpan</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Tutup
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

@push('custom-scripts')
<script src="{{ URL::asset('resources/js/admin/master-staff.js') }}"></script>
@endpush

@endsection
