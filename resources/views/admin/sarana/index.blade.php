@extends('layouts.admin._master-admin')
@section('content')

<!-- Content -->
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Master /</span> Sarana </h4>

    <!-- DataTable with Buttons -->
    <div class="card">
        <div class="card-datatable table-responsive pt-0">
            <table class="datatables-sarana table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Jenis</th>
                        <th>QTY</th>
                        <th>Satuan</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!--/ DataTable with Buttons -->

    <!-- Edit Sarana Modal -->
    <div class="modal fade" id="editSaranaModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-simple modal-edit-user">
            <div class="modal-content p-3 p-md-5">
                <div class="modal-body py-3 py-md-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center mb-4">
                        <h3 class="mb-2">Form Ubah Data Sarana</h3>
                        <p class="pt-1">Silahkan lengkapi kebutuhan data dibawah ini.</p>
                    </div>
                    <form action="{{ route('sarana.update')}}" class="row g-4" method="POST">
                        @csrf
                        <input type="hidden" name="sarana_id" value="sarana_id" id="sarana_id">
                        <div class="col-12 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="kode" name="kode" class="form-control" value="" />
                                <label for="kode">Kode Sarana</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="nama" name="nama"
                                class="form-control" value=""/>
                                <label for="nama">Nama Sarana</label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="jenis" name="jenis"
                                class="form-control"/>
                                <label for="jenis">Jenis Sarana</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="input-group input-group-merge">
                                <div class="form-floating form-floating-outline">
                                    <input type="number" id="qty" name="qty"  class="form-control phone-number-mask" value="" />
                                    <label for="qty">Jumlah Sarana / QTY</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="satuan" name="satuan"
                                class="form-control" value="" />
                                <label for="satuan">Satuan</label>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Batal
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/ Edit Sarana Modal -->

    <!-- Add Sarana Modal -->
    <div class="modal fade" id="addSaranaModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-simple modal-edit-user">
            <div class="modal-content p-3 p-md-5">
                <div class="modal-body py-3 py-md-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center mb-4">
                        <h3 class="mb-2">Form Tambah Data Sarana</h3>
                        <p class="pt-1">Silahkan lengkapi kebutuhan data dibawah ini.</p>
                    </div>
                    <form action="{{ route('sarana.store')}}" class="row g-4" method="POST">
                        @csrf
                        <div class="col-12 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="kode" name="kode" class="form-control" />
                                <label for="kode">Kode Sarana</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="nama" name="nama"
                                class="form-control" required/>
                                <label for="nama">Nama Sarana</label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="jenis" name="jenis"
                                class="form-control"/>
                                <label for="jenis">Jenis Sarana</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="input-group input-group-merge">
                                <div class="form-floating form-floating-outline">
                                    <input type="number" id="qty" name="qty"  class="form-control phone-number-mask" />
                                    <label for="qty">Jumlah Sarana / QTY</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="satuan" name="satuan"
                                class="form-control" />
                                <label for="satuan">Satuan</label>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Batal
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/ add Sarana Modal -->

</div>

@push('custom-scripts')
<script src="{{ URL::asset('resources/js/admin/master-sarana.js') }}"></script>
@endpush

@endsection
