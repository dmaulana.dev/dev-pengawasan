@extends('layouts.admin._master-admin')
@section('content')

<div class="container-xxl flex-grow-1 container-p-y">

    <div class="row invoice-add">
        <!-- surat tugas Add-->
        <div class="col-lg-12 col-12 mb-lg-0 mb-4">
            <div class="card invoice-preview-card">
                <div class="card-body">
                    <form class="" action="{{ route('surat-tugas.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <h4 style="margin-bottom: 0px;" class="p-3 mt-4">Perusahaan yang dituju</h4>
                        <div class="form-group row table-responsive">
                            <div class="card">
                                <div class="card-datatable table-responsive pt-0">
                                    <table class="datatables-spbu table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Kode</th>
                                                <th>Nama</th>
                                                <th>Pengelola</th>
                                                <th>No HP</th>
                                                <th>No. UTTP</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @if (count($temp) > 0)
                        <h4 style="margin-bottom: 0px;" class="p-3 mt-4">Informasi Perusahaan dan Staff</h4>
                        <table class="table table-bordered"  id="infoStaff" style="background-color: rgb(230, 230, 230)">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kode</th>
                                    <th>Perusahaan</th>
                                    <th>Pengelola</th>
                                    <th>No Hp</th>
                                    <th>NIB</th>
                                    <th>Staff</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach (json_decode($temp) as $key => $tempSurat)
                                <tr>
                                    <td>
                                        <input type="hidden" name="perusahaan_id[]" value="{{$tempSurat->perusahaan_id}}">
                                        <input type="hidden" name="perusahaan_kode[]" value="{{$tempSurat->perusahaan_kode}}">
                                        <input type="hidden" name="perusahaan_nama[]" value="{{$tempSurat->perusahaan_nama}}">
                                        <button type="button" class="btn btn-sm btn-secondary detailsStaff" data-id="{{$tempSurat->perusahaan_id}}"><i class="mdi mdi-information-outline"></i></button>
                                    </td>
                                    <td>{{ $tempSurat->perusahaan_kode }}</td>
                                    <td>{{ $tempSurat->perusahaan_nama }}</td>
                                    <td>{{ $tempSurat->pengelola }}</td>
                                    <td>{{ $tempSurat->nomor_hp }}</td>
                                    <td>{{ $tempSurat->nomor_nib }}</td>
                                    <td>{{ $tempSurat->jumlah_staff }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        @endif
                        <br/>
                        <br/>
                        <h4 style="margin-bottom: 0px;" class="p-3">Form Input Surat Tugas SPBU</h4>
                        <div class="row p-3">
                            <div class="col-md-4 col-4 mb-3">
                                <p class="mb-2 repeater-title">Nomor Surat</p>
                                <input type="text" class="form-control invoice-item-price" name="nomor_surat" placeholder="SPBU0010" required>
                            </div>
                            <div class="col-md-4 col-4 mb-3">
                                <p class="mb-2 repeater-title">Objek Pengawasan</p>
                                <input type="text" class="form-control invoice-item-price" name="objek_pengawasan" placeholder="SPBU" required>
                            </div>
                            <div class="col-md-4 col-4 mb-3">
                                <p class="mb-2 repeater-title">Perihal Surat</p>
                                <input type="text" class="form-control invoice-item-price" placeholder="surat penugasan spbu" name="perihal" required>
                            </div>
                            <div class="col-md-6 mb-4">
                                <div class="form-floating form-floating-outline">
                                    <p class="mb-2 repeater-title">Sarana</p>
                                    <select id="select2Multiple" name="sarana[]" class="select2 form-select" multiple required>
                                        <optgroup label="Nama Sarana">
                                            @foreach ($sarana as $item)
                                            <option value="{{ $item->kode }}">{{ $item->nama }}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-3 mb-3">
                                <p class="mb-2 repeater-title">Tanggal Surat</p>
                                <input type="date" class="form-control date-picker flatpickr-input active" name="tanggal_surat" required>
                            </div>
                            <div class="col-md-3 col-3 mb-3">
                                <p class="mb-2 repeater-title">Tanggal Pengawasan</p>
                                <input type="date" class="form-control date-picker flatpickr-input active" name="tanggal_pelaksanaan" required>
                            </div>
                            <div class="col-md-12 col-12 mb-3">
                                <p class="mb-2 repeater-title">Tentang</p>
                                <textarea class="form-control" name="tentang"></textarea>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-12 p-4">
                                <button type="submit" class="btn btn-primary">
                                    SIMPAN
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="modalPetugasAdd" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-simple">
            <div class="modal-content p-3 p-md-5">
                <div class="modal-body py-3 py-md-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center mb-4">
                        <h3 class="mb-2">Tambah Petugas Lapangan</h3>
                        <p class="pt-1">Anda Dapat menambahkan staff lebih dari satu.</p>
                    </div>
                    <form action="{{route('surat-tugas.temp')}}" class="row g-4" method="POST">
                        @csrf
                        <input type="hidden" id="perusahaan_id" name="perusahaan_id" class="form-control" value="" />
                        <table id="item_table" class="table" align=center>
                            <thead>
                                <tr>
                                    <th class="border-bottom-primary" style="padding-left: 10px;">Nama Lengkap</th>
                                    <th class="border-bottom-primary" style="padding-left: 10px;">NRK</th>
                                    <th class="border-bottom-primary" style="padding-left: 10px;">Jabatan</th>
                                    <th class="border-bottom-primary" style="padding-left: 10px;">Unit Kerja</th>
                                    <th class="border-bottom-primary" style="padding-left: 10px;"> </th>
                                </tr>
                            </thead>
                            <style type="text/css">
                                .source-item td {
                                    padding: 5px;
                                }
                            </style>
                            <tbody id="tbody-staff">
                                <tr class="border-bottom-primary source-item">
                                    <td>
                                        <select id="select2Basic" class="select2 form-select form-control dynamic" data-allow-clear="true" name="staff_id[]" required="">
                                            <option selected disabled>Select Nama Staff</option>
                                            @foreach ($staff as $sf)
                                            <option value="{{$sf->id}}">{{$sf->nama_lengkap}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input type="number" name="nomor_nip[]" class="form-control targetNrk" id="targetNrk" placeholder="nomor_nip" required="" value="">
                                    </td>
                                    <td>
                                        <input type="text" name="jabatan[]" class="form-control targetJabatan" id="targetJabatan" placeholder="jabatan" required="" value="">
                                    </td>
                                    <td>
                                        <input type="text" name="unit_kerja[]" class="form-control targetUnit" id="targetUnit" placeholder="Unit Kerja" readonly="" value="">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary me-sm-3 me-1">Simpan</button>
                            <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Batal
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalDetailsStaff" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-simple">
            <div class="modal-content p-3 p-md-5">
                <div class="modal-body py-3 py-md-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="text-center mb-4">
                        <h4 class="mb-2">Informasi Details Petugas Lapangan</h4>
                    </div>
                    <div class="card-datatable table-responsive pt-0">
                        <table class="datatables-temp-details table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nomor NIP</th>
                                    <th>Nama</th>
                                    <th>Unit Kerja</th>
                                    <th>Jabatan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    $(document).ready(function() {
        var count = 1;
        dynamic_field(count);

        function dynamic_field(number) {
            html = '<tr class="source-item">';
                html +=
                '<td><select id=select2Basic" class="select2 form-select form-control dynamic" data-allow-clear="true" name="staff_id[]" required=""><option selected disabled>Select Nama Staff</option>@foreach ($staff as $sf)<option value="{{$sf->id}}">{{$sf->nama_lengkap}}</option>@endforeach</select></td>';
                html +=
                '<td><input type="text" name="nomor_nip[]" class="form-control targetNrk" placeholder="nomor_nip" required="" value=""></td>';
                html +=
                '<td><input type="text" name="jabatan[]" class="form-control targetJabatan" id="targetJabatan" placeholder="Jabatan" readonly="" required="" value=""></td>';
                html +=
                '<td><input type="text" name="unit_kerja[]" class="form-control targetUnit" id="unitKerja" placeholder="Unit Kerja" readonly="" required="" value=""></td>';
                if (number > 1) {
                    html +=
                    '<td><button type="button" name="remove" id="" class="btn btn-danger remove">-</button></td></tr>';
                    $('#tbody-staff').append(html);
                } else {
                    html +=
                    '<td><button type="button" name="add" id="add" class="btn btn-success"><i class="fa fa-plus"><i/></button></td></tr>';
                        $('#tbody-staff').html(html);
                    }
                }

                $(document).on('click', '#add', function() {
                    count++;
                    dynamic_field(count);
                });

                $(document).on('click', '.remove', function() {
                    count--;
                    $(this).closest("tr").remove();
                });


                $(document).ready(function() {
                    $('#item_table').on('change', '.dynamic', function(e) {
                        e.preventDefault()

                        const idVal = e.target.value;
                        let _this = this;

                        $.ajax({
                            url: `/master-staff/show/${idVal}`,
                            type: "GET",
                            cache: false,
                            success: function(data) {
                                var data = data.data
                                const Nrk = $(_this).closest('tr').find('.targetNrk');
                                $(Nrk).val(data.nomor_nip)
                                const Jabatan = $(_this).closest('tr').find('.targetJabatan');
                                $(Jabatan).val(data.jabatan)
                                const Unit = $(_this).closest('tr').find('.targetUnit');
                                $(Unit).val(data.unit_kerja)
                            },
                            error: function(err) {
                                console.log('err data ', err)
                            }
                        })
                    });
                });

            });
        </script>



        @push('custom-scripts')
        <script src="{{ URL::asset('resources/js/admin/surat-tugas-create.js') }}"></script>
        <script src="{{URL::asset('public/admin/assets/js/forms-selects.js')}}"></script>
        @endpush

        @endsection
