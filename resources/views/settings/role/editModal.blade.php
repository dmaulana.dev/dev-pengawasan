<!-- Edit Role Modal -->

    <div class="modal-dialog modal-lg modal-dialog-centered modal-edit-new-role">
        <div class="modal-content p-3 p-md-5">
            <form method="POST" action="{{ route('role.update', ['id' => $data->id]) }}">
                @csrf
                <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body p-md-0">
                    <div class="text-center mb-4">
                        <h3 class="role-title mb-2 pb-0">Ubah Role Anda</h3>
                        <p>Set izin role menu</p>
                    </div>
                    <div class="col-12 mb-4">
                        <div class="form-floating form-floating-outline mb-3">
                            <input type="text" id="editRoleName" name="editRoleName" class="form-control"
                                value="{{$data->name}}"
                                placeholder="Masukkan Nama Role" tabindex="-1" />
                            <label for="editRoleName">Nama Role</label>
                        </div>
                        <div class="form-floating form-floating-outline">
                            <input type="text" id="editRoleDesc" name="editRoleDesc" class="form-control"
                                value="{{$data->description}}"
                                placeholder="Masukkan Keterangan" tabindex="-1" />
                            <label for="editRoleDesc">Keterangan</label>
                        </div>
                    </div>
                    <div class="col-12">
                        <h5>Pengaturan Izin Role / Menu</h5>
                        <!-- Permission table -->
                        <div class="table-responsive">
                            <table class="table table-flush-spacing">
                                <tbody>
                                    @for ($i = 0; $i < count($permission); $i++)
                                        {{-- Parent --}}
                                        @if ($permission[$i]->parent_id == null)
                                            <tr>
                                                <td class="text-nowrap fw-semibold">{{ $permission[$i]->name }}
                                                </td>
                                                <td>
                                                    <div class="d-flex">
                                                        <div class="form-check me-3 me-lg-5">
                                                            <input class="form-check-input parent-input"
                                                                type="checkbox" value="{{ $permission[$i]->id }}"
                                                                id="parent-select{{ $permission[$i]->id }}"
                                                                {{ $list_id->contains($permission[$i]->id) ? "checked" : '' }}
                                                                name="permission[]" />
                                                            <label class="form-check-label" for="parent-select{{ $permission[$i]->id }}">
                                                                {{ $permission[$i]->name }}</label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif

                                        {{-- Child --}}
                                        @if ($permission[$i]->parent_id != null)
                                            <tr>
                                                <td class="text-nowrap fw-semibold"></td>
                                                <td class="ps-5">
                                                    <div class="d-flex">
                                                        <div class="form-check me-3 me-lg-5">
                                                            <input class="form-check-input child-input parent-select{{ $permission[$i]->parent_id }}"
                                                                type="checkbox"
                                                                id="child-select{{ $permission[$i]->id }}"
                                                                data-parent_id="parent-select{{ $permission[$i]->parent_id }}"
                                                                value="{{ $permission[$i]->id }}"
                                                                {{ $list_id->contains($permission[$i]->id) ? "checked" : '' }}
                                                                name="permission[]" />
                                                            <label class="form-check-label"
                                                                for="child-select{{ $permission[$i]->id }}">
                                                                {{ $permission[$i]->name }}</label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                        <!-- Permission table -->
                    </div>
                    <div class="col-12 text-center">
                        <button type="submit" id="btnSubmit"
                            class="btn btn-primary btn-submit me-sm-3 me-1">Submit</button>
                        <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                            aria-label="Close">Cancel</button>
                    </div>
                    <!--/ Edit role form -->
                </div>
            </form>
        </div>
    </div>

<script>
    $('#editRoleModal .parent-input').on('click', function(){
        const id = $(this).attr('id');
        const checked = $(this).prop('checked');

        $(`#editRoleModal .${id}`).each((idx) => {
            $($(`#editRoleModal .${id}`)[idx]).prop('checked', checked);
        })
    })

    $('#editRoleModal .child-input').on('click', function(){
        const parentID = $(this).data('parent_id');
        const checked = $(this).prop('checked');
        if($(`#editRoleModal #${parentID}`).prop('checked') === false){
            $(`#editRoleModal #${parentID}`).prop('checked', checked);
        }
    })

    // $("#editRoleModal .parent-input").click(function() {
    //     let parentId = $(this).val();

    //     const selectCheckBox = document.querySelector('#editRoleModal #parent-select' + parentId),
    //         checkboxList = document.querySelectorAll("#editRoleModal #child-select" + parentId, ".child-input",
    //             "[type='checkbox']");
    //     selectCheckBox.addEventListener('change', t => {
    //         checkboxList.forEach(e => {
    //             e.checked = t.target.checked;
    //         });
    //     });
    // });
</script>

<!--/ Edit Role Modal -->