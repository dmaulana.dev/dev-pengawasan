<?php

use Illuminate\Support\Facades\Route;

//admin
use App\Http\Controllers\Admin\MSpbuController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\ConfigurationController;
use App\Http\Controllers\Admin\MAdminController;
use App\Http\Controllers\Admin\MStaffController;
use App\Http\Controllers\Admin\MUserController;
use App\Http\Controllers\Admin\MSaranaController;
use App\Http\Controllers\Admin\SuratTugasController;
use App\Http\Controllers\Admin\BeritaAcaraController;
use App\Http\Controllers\Admin\MAlkoholController;
use App\Http\Controllers\Admin\DashboardAdminController;

// user controller
use App\Http\Controllers\User\DashboardUserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// admin route
Route::get('/', [AuthController::class, 'index'])->name('auth.login');
Route::get('/login', [AuthController::class, 'index'])->name('auth.login');

Route::middleware(['has_login'])->group(function () {
    // Dashboard Admin
    Route::get('/dashboard-admin', [DashboardAdminController::class, 'index'])->name('dashboard.admin');

    // master Data admin
    Route::get('/master-admin', [MAdminController::class, 'index'])->name('master-admin.index');
    Route::get('/master-admin/get-data', [MAdminController::class, 'getDataAdmin'])->name('master-admin.get-data');
    Route::post('/master-admin/store', [MAdminController::class, 'store'])->name('master-admin.store');
    Route::get('/master-admin/details/{id}', [MAdminController::class, 'details'])->name('master-admin.details');
    Route::get('/master-admin/show/{id}', [MAdminController::class, 'show'])->name('master-admin.show');
    Route::post('/master-admin/update', [MAdminController::class, 'update'])->name('master-admin.update');
    Route::get('/master-admin/destroy/{id}', [MAdminController::class, 'destroy'])->name('master-admin.destroy');

    // master data staff
    Route::get('/master-staff', [MStaffController::class, 'index'])->name('master-staff.index');
    Route::post('/master-staff/store', [MStaffController::class, 'store'])->name('master-staff.store');
    Route::get('/master-staff/show/{id}', [MStaffController::class, 'show'])->name('master-staff.show');
    Route::post('/master-staff/update', [MStaffController::class, 'update'])->name('master-staff.update');
    Route::get('/master-staff/destroy', [MStaffController::class, 'destroy'])->name('master-staff.destroy');

    //master spbu
    Route::get('/master-spbu', [MSpbuController::class, 'index'])->name('master-spbu.index');
    Route::post('/master-spbu/store', [MSpbuController::class, 'store'])->name('master-spbu.store');
    Route::get('/master-spbu/show/{id}', [MSpbuController::class, 'show'])->name('master-spbu.show');
    Route::post('/master-spbu/update', [MSpbuController::class, 'update'])->name('master-spbu.update');
    Route::get('/master-spbu/destroy', [MSpbuController::class, 'index'])->name('master-spbu.index');

    //user or pengguna
    Route::get('/dashboard-user', [DashboardUserController::class, 'index'])->name('dashboard.user');

    // master user
    Route::get('/master-user', [MUserController::class, 'index'])->name('master-user.index');
    Route::get('/master-user/get-data', [MUserController::class, 'getDataUser'])->name('master-user.get-data');
    Route::post('/master-user/store', [MUserController::class, 'store'])->name('master-user.store');
    Route::get('/master-user/show/{id}', [MUserController::class, 'show'])->name('master-user.show');
    Route::post('/master-user/update/{id}', [MUserController::class, 'update'])->name('master-user.update');
    Route::get('/master-user/destroy/{id}', [MUserController::class, 'destroy'])->name('master-user.destroy');

    // config
    Route::get('/configuration', [ConfigurationController::class, 'index'])->name('configuration.index');

    // Permission
    Route::get('/permission', [PermissionController::class, 'index'])->name('permission.index');
    Route::get('/permission/get-data', [PermissionController::class, 'getData'])->name('permission.get-data');
    Route::post('/permission/store', [PermissionController::class, 'store'])->name('permission.store');
    Route::get('/permission/show/{id}', [PermissionController::class, 'show'])->name('permission.show');
    Route::post('/permission/updated', [PermissionController::class, 'update'])->name('permission.update');
    Route::get('/permission/destroy/{id}', [PermissionController::class, 'destroy'])->name('permission.destroy');

    // role
    Route::get('/role', [RoleController::class, 'index'])->name('role.index');
    Route::get('/role/get-role', [RoleController::class, 'getData'])->name('role.get-data');
    Route::post('/role/store', [RoleController::class, 'store'])->name('role.store');
    Route::get('/role/show/{id}', [RoleController::class, 'show'])->name('role.show');
    Route::get('/role/show-edit/{id}', [RoleController::class, 'showedit'])->name('role.show-edit');
    Route::post('/role/update/{id}', [RoleController::class, 'update'])->name('role.update');
    Route::get('/role/destroy/{id}', [RoleController::class, 'destroy'])->name('role.destroy');


    //transaksi
    Route::get('/surat-tugas', [SuratTugasController::class, 'index'])->name('surat-tugas.index');
    Route::get('/surat-tugas/create', [SuratTugasController::class, 'create'])->name('surat-tugas.create');
    Route::post('/surat-tugas/store', [SuratTugasController::class, 'store'])->name('surat-tugas.store');
    Route::get('/surat-tugas/details/{id}', [SuratTugasController::class, 'details'])->name('surat-tugas.details');
    Route::post('/surat-tugas/temp', [SuratTugasController::class, 'temp'])->name('surat-tugas.temp');
    Route::get('/surat-tugas/temp-details/{id}', [SuratTugasController::class, 'tempDetails'])->name('surat-tugas.temp-details');
    Route::get('/surat-tugas/temp-hapus/{id}', [SuratTugasController::class, 'tempHapus'])->name('surat-tugas.temp-hapus');
    Route::get('/surat-tugas/get-spbu', [SuratTugasController::class, 'getSpbu'])->name('surat-tugas.get-spbu');

    Route::get('/berita-acara', [BeritaAcaraController::class, 'index'])->name('berita-acara.index');

    Route::get('/sarana', [MSaranaController::class, 'index'])->name('sarana.index');
    Route::get('/sarana/show/{id}', [MSaranaController::class, 'show'])->name('sarana.show');
    Route::post('/sarana/update', [MSaranaController::class, 'update'])->name('sarana.update');
    Route::post('/sarana/store', [MSaranaController::class, 'store'])->name('sarana.store');
    Route::get('/sarana/destroy/{id}', [MSaranaController::class, 'destroy'])->name('sarana.destroy');


    Route::get('/master-minuman-alkohol', [MAlkoholController::class, 'index'])->name('minuman-alkohol.index');

    // Logout
    Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');
});

Route::get('/404', function(){
    abort(404);
})->name('not_found');

